package com.example.taskmanagement;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class CreateScreen extends AppCompatActivity {
Button btnCreate;
EditText editText;
Button btnUpdate;
    TaskAdapter adapter;
    TaskDatabase db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_screen);
        btnCreate = findViewById(R.id.btnCreate);
        btnUpdate = findViewById(R.id.btnUpdate);
        editText = findViewById(R.id.editText);
        db = Room.databaseBuilder(getApplicationContext(),
                TaskDatabase.class, "database-name").build();
        btnCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new AsyncTask<Void, Void, Void>() {
                    @Override
                    protected Void doInBackground(Void... voids) {

                        @SuppressLint("WrongThread") Task task = new Task(String.valueOf(editText.getText()));
                        db.taskDao().insert(task);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Intent intent = new Intent(CreateScreen.this, ListScreen.class);
                                startActivity(intent);
                            }
                        });

                        return null;
                    }
                }.execute();
            }
        });






    }

    void updateItem(final int updatePosition){
        final Task updateTask = adapter.listTask.get(updatePosition);
        new AsyncTask<Void,Void,Void>(){
            @Override
            protected Void doInBackground(Void... voids) {
                Log.i("this", String.valueOf(updatePosition));
                db.taskDao().update(updateTask);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                adapter.listTask.add(updateTask);
                adapter.notifyDataSetChanged();
            }
        }.execute();
    }
}
