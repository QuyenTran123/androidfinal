package com.example.taskmanagement;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.util.List;

public class ListScreen extends AppCompatActivity {
    TaskDatabase db;
    RecyclerView recyclerView;
    TaskAdapter adapter;
    Button btnAdd;

    @SuppressLint("StaticFieldLeak")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_screen);

        db = Room.databaseBuilder(getApplicationContext(),
                TaskDatabase.class, "database-name").build();
        btnAdd = findViewById(R.id.btnAdd);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ListScreen.this, CreateScreen.class);
                startActivity(intent);
            }
        });

        recyclerView = findViewById(R.id.re_task);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        adapter = new TaskAdapter();
        adapter.listener = new TaskAdapter.OnItemListener() {
            @Override
            public void onDeleteClicked(int position) {
                deleteItem(position);
            }

            @Override
            public void onUpdateClicked(int position) {

                updateScreen(adapter.listTask.get(position));
            }
        };
        recyclerView.setAdapter(adapter);

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                final List<Task> tasks = db.taskDao().getAll();
                Log.i("Tags", "size " + tasks.size());

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        display(tasks);
                    }
                });
                return null;
            }
        }.execute();
    }


    void deleteItem(final int deletePosition) {
        final Task deleteTask = adapter.listTask.get(deletePosition);
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                Log.i("this", String.valueOf(deletePosition));
                db.taskDao().delete(deleteTask);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                adapter.listTask.remove(deletePosition);
                adapter.notifyDataSetChanged();
            }
        }.execute();
    }

    public void display(List<Task> tasks) {
        recyclerView = findViewById(R.id.re_task);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        adapter.listTask = tasks;
        recyclerView.setAdapter(adapter);

    }

    public void updateScreen(Task tasks) {
        Intent intent = new Intent(ListScreen.this, Update.class);
        intent.putExtra("id", String.valueOf(tasks.getId()));
        intent.putExtra("task", tasks.getContent());
        startActivity(intent);
    }


}
