package com.example.taskmanagement;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.icu.util.LocaleData;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import static java.lang.Integer.parseInt;

public class Update extends AppCompatActivity {
    TaskDatabase db;
    Button btnUpdate;
    EditText editText;
    Button btn_save;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update2);
        db = Room.databaseBuilder(getApplicationContext(),
                TaskDatabase.class, "database-name").build();
        btnUpdate = findViewById(R.id.btnUpdate);
        editText = findViewById(R.id.editTextupdate);


        final String taskUpdated = getIntent().getStringExtra("task");
        final String id = getIntent().getStringExtra("id");
        editText.setText(taskUpdated);


        btnUpdate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                final String edit_name = editText.getText().toString();
                new AsyncTask<Void, Void, Void>() {
                    @SuppressLint("WrongThread")
                    @Override
                    protected Void doInBackground(Void... voids) {
                        Task task = new Task(edit_name);
                        task.setId(parseInt(id));
                        db.taskDao().update(task);

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Intent intent = new Intent(Update.this, ListScreen.class);
                                startActivity(intent);
                            }
                        });

                        return null;
                    }
                }.execute();
            }
        });

    }




}
